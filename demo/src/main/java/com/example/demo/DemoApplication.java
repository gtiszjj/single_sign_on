package com.example.demo;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
@SpringBootApplication
public class DemoApplication {

//	@RequiresPermissions(value = "user:info")
//	@GetMapping(value = "/users/{id}")
//	public PrincipalCollection getUserById() {
//		PrincipalCollection principalCollection = SecurityUtils.getSubject().getPrincipals();
//		return principalCollection;
//	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
