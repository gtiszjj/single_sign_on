package com.example.demo;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class IndexController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping({ "/", "" })
	String welcome(Model model) {
		return "redirect:/index";
	}

	@GetMapping({ "/index" })
	String index(HttpServletRequest request,Model model) {
		//用户详细信息
		Principal principal = request.getUserPrincipal();
//		Object principal = SecurityUtils.getSubject().getPrincipal();
		model.addAttribute("user", principal);
		return "index_v1";
	}

}
