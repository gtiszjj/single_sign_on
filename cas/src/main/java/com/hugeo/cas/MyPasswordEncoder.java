package com.hugeo.cas;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author anumbrella
 */
@Service
public class MyPasswordEncoder implements PasswordEncoder {
    private static final String SALT = "1qazxsw2";
    private static final String ALGORITH_NAME = "md5";
    private static final int HASH_ITERATIONS = 2;

    @Override
    public String encode(CharSequence charSequence) {
        // charSequence为输入的用户密码
        String username = getUserName();
        charSequence = new SimpleHash(ALGORITH_NAME, charSequence, ByteSource.Util.bytes(username + SALT),
                HASH_ITERATIONS).toHex();
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String str) {
        // 当encode方法返回不为null时，matches方法才会调用，charSequence为encode返回的字符串
        // str字符串为数据库中密码字段返回的值
        String encodeStr = charSequence.toString();
        String username = getUserName();
        encodeStr = new SimpleHash(ALGORITH_NAME, encodeStr, ByteSource.Util.bytes(username + SALT),
                HASH_ITERATIONS).toHex();
        if (encodeStr.equals(str)) {
            return true;
        }
        return false;
    }

    public String getUserName(){
        RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
        String username = request.getParameter("username").toString();
        return username;
    }
}