# 单点登录

#### 项目介绍

一个简单的集成了shiro+cas+pac4j的springboot项目，实现单点登录及单点退出。
包括一个cas-server服务器和一个demo客户端

#### 开发环境

java1.7以上 
intellij idea
maven
mysql
tomcat

#### 使用说明

1. 准备一个纯净的tomcat,修改端口号为8444
2. 修改cas-server的数据库连接并打包部署到tomcat
3. 修改demo的application.yml配置文件，包括端口号8085，clientName为demo,cas请求url，和认证后返回的url
![输入图片说明](https://images.gitee.com/uploads/images/2018/0905/174118_578597be_1027623.png "TIM图片20180905174133.png")
4.把demo项目打包成demo.jar文件并通过命令java -jar demo.jar运行
5.重复步骤3，4，修改端口号为8086，clientName为demo1打包并运行
6.访问demo，demo1后其中一个登录后另一个自动登录，退出亦然。

#### 演示地址

1. http://47.94.83.185:8085/demo
2. http://47.94.83.185:8086/demo1
3. http://47.94.83.185:8444/cas
登录账户：admin 密码：111111

#### 注意事项

1. 如果数据库用户用了指定的加密算法加密，在登录验证的时候要使用同样的算法
2. cas默认是https协议，要改为http，需要修改cas-server的配置文件application.properties，把有关ssl的注释掉并添加
    cas.tgc.secure=false
    cas.warningCookie.secure=false
3. cas-server可自定义登录页，添加验证码等

#### 参考文档
https://www.cnblogs.com/suiyueqiannian/p/9359597.html



